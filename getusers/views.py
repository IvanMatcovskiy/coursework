from django.shortcuts import render
import vk
import time

def search_form(request):
    return render(request, 'getusers/search_form.html')

def search(request):

    if 'id_group' in request.GET and request.GET['id_group']:
        session = vk.Session()
        api = vk.API(session)
        
        gr_id = request.GET['id_group']  # id of group

        #collect all users of group
        shift = 0
        l = []
        while True :
            try:
                response = api.groups.getMembers (group_id = gr_id,  offset = shift)
                shift += len(response['users'])
                l.extend(response['users'])
                if shift == response['count']:
                    break
            except:
                print('waiting')
                
        info = {'id' : l, 'quntity' : shift}
        return render(request, 'getusers/search.html', info)

    else:
        return render(request, 'getusers/search_form.html')    